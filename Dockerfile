FROM tomcat:8.0
WORKDIR /usr/local/tomcat/webapps
RUN pwd
RUN ls -lart
COPY petclinic.war /usr/local/tomcat/webapps/petclinic.war
RUN ls -lart